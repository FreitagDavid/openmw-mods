# Copyright 2023 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Pete's Scroll 2018 ...in 2020"
    DESC = " 1k, 2k, and 4k replacements for the scroll texture, with optional daedric alphabet."
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/47863"
    KEYWORDS = "openmw"
    LICENSE = "attribution-derivation-nc"
    TEXTURE_SIZES = "1024 2048 4096"
    NEXUS_SRC_URI = """
        https://nexusmods.com/morrowind/mods/47863?tab=files&file_id=1000018109
        -> Pete\'s_Scroll-47863-1-0-1587634237.7z
    """
    INSTALL_DIRS = [
        InstallDir(
            "01 Scroll - 1K",
            REQUIRED_USE="texture_size_1024",
        ),
        InstallDir(
            "01 Scroll - 2K",
            REQUIRED_USE="texture_size_2048",
        ),
        InstallDir(
            "01 Scroll - 4K",
            REQUIRED_USE="texture_size_4096",
        ),
    ]
