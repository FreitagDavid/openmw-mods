# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Shacks Docks and Ships - Arkitektora of Vvardenfell"
    DESC = "Contains new textures for wooden shacks, docks and ships"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/43520"
    NEXUS_URL = HOMEPAGE
    KEYWORDS = "openmw"
    # permission obtained by marius851000
    LICENSE = "all-rights-reserved-mirror-permission"
    TEXTURE_SIZES = "1024 2048"
    SRC_URI = """
        texture_size_1024? ( MQ_Textures-43520-1-0.7z )
        texture_size_2048? ( HQ_Textures-43520-1-0.7z )
    """
    # modding-openmw.com recommends removing these files to avoid overriding other
    # mods that provide wood textures
    BLACKLIST = [
        "Textures/Tx_wood_brown_shelf_corner.dds",
        "Textures/Tx_wood_brown_shelf_corner_01.dds",
        "Textures/Tx_wood_brown_shelf.dds",
    ]
    INSTALL_DIRS = [
        InstallDir(
            "MQ/Data Files",
            S="MQ_Textures-43520-1-0",
            BLACKLIST=BLACKLIST,
            REQUIRED_USE="texture_size_1024",
        ),
        InstallDir(
            "HQ/Data Files",
            S="HQ_Textures-43520-1-0",
            BLACKLIST=BLACKLIST,
            REQUIRED_USE="texture_size_2048",
        ),
        # textures for Windows glow
        # InstallDir("MQ/Extras/Windows Glow", S="MQ_Textures-43520-1-0"),
        # InstallDir("HQ/Extras/Windows Glow", S="HQ_Textures-43520-1-0"),
    ]
